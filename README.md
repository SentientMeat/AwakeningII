# Awakening II: The Resurrection
This partial conversion mod for id Software's **_Quake II_** is a sequel to the original Awakening mod by Eric "redchurch" von Rothkirch and Patrick Martin.

## Authors
* Chris Walker — _Lead Developer_
* David "Zoid" Kirsch — _Quake II CTF_
* Ponpoko — _3ZB2 Bots_

With additional coding contributions by:
* David Hyde
* Richard Stanway
* Doug "Raven" Buckley

## Game Notes
This mod requires v3.20 or higher of Quake II to run. Full instructions along with the game assets used by the mod can be downloaded here (including the Linux version):<br>
http://awakening2.backshooters.com/

## Dev Notes
The original baseline for the mod was the Quake II CTF v1.50 game DLL source code (1998). It's had a lot ripped out and a lot more added – not just from me, but from several other authors as well who have kindly allowed their code to be used in this project. My changes are denoted by '//CW' tags.

The project files are for the Microsoft Visual C++ 6.0 IDE, should you wish to make use of them. (Compiles the _gamex86.dll_ file for the Windows platform.)

## Licence
id Software later released the full Quake II source code under the GNU General Public License, and it can be found here:<br>
https://github.com/id-Software/Quake-2

Although the original mod source code files were not under the GPL at the time, I have taken the liberty of adding the licence in keeping with the spirit of id Software's ethos. Feel free to make use of the code as you like. Please give credit if you use any of the code in your own applications or mods. It is provided as-is with no warranties of any kind.

**GPL From id Software:**
```
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
```
