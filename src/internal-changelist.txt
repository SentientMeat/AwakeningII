-------------------
v1.99.6
-------------------
- Weapon model pointers are used once again for modelindexes, rather than direct 
  path (which was added in v1.99.5).
- Removed some R_CullAliasModel lines & comments from v1.99.5.


-------------------
v1.99.12
-------------------
- Re-made it so that frozen players can't be moved even using the AGM.


-------------------
v1.99.13
-------------------
- Increased vec3_border from {1, 1, 1} to {10, 10, 10}.
- CTF yes/no vote commands commented out again.


-------------------
v1.99.14
-------------------
- Removed 'agm_death' flagging from v1.99.11 in ClientObituary().
- Decreased vec3_border from {10, 10, 10} to {4, 4, 4}.


-------------------
v1.99.15
-------------------
- Players can't detonate C4 when in Observer mode.


-------------------
v1.99.18
-------------------
- Removed solid-start check in fire_tracer() as it's done elsewhere now.


-------------------
v1.99.20
-------------------
- Separate ifs in ChangeWeapon() -> if...else if.
- Changed some hardcoded values to #defines.
- Fireball_Fizzle() -> Fireball_Think(); removed gi.linkentity() call.


-------------------
v1.99.21
-------------------
- Removed AI_SetSightClient() call in G_RunFrame().


-------------------
v1.99.22
-------------------
- Tweaked the drawing of the Gauss Pistol particle beam.


-------------------
v1.99.23
-------------------
- Removed 'admin' client command; superseded by Op status.
- Replaced 'boot' client command with 'op_kick'.
- Moved Open*File() functions from g_main.c to g_misc.c.
- Replaced maphack's stricmp() calls with Q_stricmp() calls.


-------------------
v1.99.24
-------------------
- Used floating-point harcoded numbers instead of integers for:
    vectoyaw(), vectoangles(), vec3_origin, vec3_up, vec3_dn, vec3_border, 
	VEC_UP, MOVEDIR_UP, VEC_DOWN, MOVEDIR_DOWN
- Removed 'g_ops' stuff.
- Various SVF_NOCLIENT stuff to help reduce server-client traffic.
- Tracers spawned less frequently.


-------------------
v1.99.25
-------------------
- Added INFO messages for use of the 'op' command.


-------------------
v1.99.28
-------------------
- Tweaked Trap-breaking alogorithm in teleporter_touch().
- Removed op-voting stuffcmds for Op menu.


-------------------
v1.99.29
-------------------
- Moved ShowMOTD() to 'g_misc.c' from 'g_ctf.c'.
- Removed an extraneous line of non-info text from the motd menu.


-------------------
v1.99.30
-------------------
- Changed /s to *s for func_explosive & misc_explobox.
- Added classnames for various entities.
- Removed "it_ent->disintegrated" hack when giving disabled items.
- Moved 'aliases_set' from client->resp to client->pers.
- Added (int) typecasts for time info sent to clients.
- Changed type of 'speed' parameter of fire_rocket() from int to float.


-------------------
v1.99.31
-------------------
- Moved 'aliases_set' back to client->resp from client->pers.


-------------------
v1.99.33
-------------------
- Removed the 'free(motd[i].text' loop at the end of PrintMOTD().
- Maplist array entries in Cmd_Vote_f() are now free()'d after being used.
- Removed pre-caching of "infantry/inflies1.wav" in SP_worldspawn().


-------------------
v1.99.35
-------------------
- "DM only" tweaks.
- Cleaned up some code in Trap_ShootHook().
- Used specific #defines for MG turret h- and v-spreads.
- Removed unnecessary precaching of 'g_scan' and 'g_targ' in SP_worldspawn().
- Corrected the use of gi.imageindex() in PrecacheItem().
- Changed default values of 'maxclients' and 'deathmatch' in 'g_save.c'.
- New function: vtosf().
- New functiom: _sn().


-------------------
v1.99.36
-------------------
- Various code tidying.
- Tweaked the horizontal placement of the help/weapon HUD icon.


-------------------
v1.99.37
-------------------
- Various code tidying.


-------------------
v1.99.38
-------------------
- Open*File() functions return immediately if the relevant file string is empty).


-------------------
v1.99.39
-------------------
- Player chase-cam pointer now cleared when they disconnect.
- Added (int) typecasting for maxclients->value references.
- "Invisible client" circumstances: player is connected, and quits whilst an observer. The next player
  that connects into their slot will still have SVF_NOCLIENT set, so if forcejoin is set, they will be 
  invisible until they die & respawn.


-------------------
v1.99.40
-------------------
- Changed some stricmp() calls to Q_stricmp().
- Various code tidying.
- Removed "#if 0" statements in CTFScoreboardMessage().


-------------------
v1.99.41
-------------------
- Removed blocks of commented-out code:
  'g_ctf.c', 'g_main.c', 'p_client.c', 'g_monster.c' and 'g_turret.c' especially.
- Cleared modelindex4 in CopyToBodyQue() - re. dead body halo bug.
- 'sv_allow_hook' and 'sv_hook_offhand'calls typecasted to int.


-------------------
v1.99.42
-------------------
- check_dodge() is no longer static.
- Tweaked bug reporting in G_FreeEdict().


-------------------
v1.99.43
-------------------
- Moved bodyque entity check to BecomeExplosion1() and BecomeExplosion2() from turret_blocked().
- Changed "/ 2" to "* 0.5" for the AccelerationDistance macro.
- Yet more optimisation tweaks and code tidying.
- Added SVF_NOCLIENT svflag for info_notnull, info_intermission, info_player_start and info_player_team*.
- Random powerup spawning functions are no longer static.
- Fixed crash vulnerabilty in CTFAssignSkin().


-------------------
v2.01
-------------------
- The particle_beam entity is now a SVF_PROJECTILE.
- More SVF_NOCLIENT tweaks.
- Added (int) typecast for "sv_gametype->value > X".
- Added flame expiration sanity check to player_die().
- Removed unnecessary single-player, co-op and monster-related code.
  & Removed medic stuff and monster_death_use call in Killed().
  & Deleted point_combat, trigger_monsterjump, target_crosslevel_trigger, target_crosslevel_target, target_goal,
    target_secret, target_help, target_lightramp, misc_deadsoldier, and viewthing entities.
  & Removed check_dodge().
  & Removed Cmd_Help_f() and HelpComputer().
  & Removed 'g_ai.c', 'm_move.c' and 'p_trail.c', and any associated function calls in other modules.
  & Moved necessary functions in 'm_move.c' to 'g_monster.c'.
  & Cleaned up 'g_monster.c'.
- Removed the 'deathmatch', 'coop' and 'skill' cvars.
  & Removed deathmatch check in respawn().
  & Removed a deathmatch check in Cmd_Score_f().
- Changed powerup framenums from floats to ints.
- Removed various #defines in 'g_local.h'.
- Removed "extern qboolean is_quad" in 'g_local.h'.
- Removed 'oldcount' from Pickup_Ammo().
- Set default sounds for func_water (for out-of-range 'sounds' value).
- CTF spawns are removed for non-CTF games.
- Moved menu and voting stuff into 'g_menus.c'.
- Culled unused obituaries.
- Removed OnSameTeam() and *ClientTeam(); tweaked Cmd_Say_f() accordingly.
- Firebomb_Touch() and T_RadiusDamage() now spawn flame entities at the client's origin.


-------------------
v2.02-beta.1
-------------------
- Various weapon projectiles now have the RF_FULLBRIGHT renderfx flag set.
- Added SVF_DEADMONSTER svflags to certain weapon projectiles.
- Removed VectorClear(mins/maxs) from weapon projectiles.
- Rearranged the selection order for NoAmmoWeaponChange().
- Spikes that touch dead bodies are now MOVETYPE_TOSS.
- Tidied up 'p_weapon.c'.
- Removed fire_hit().
- Removed VectorNormalize() calls from fire_*() functions.
- Normalized the aiming direction in ESG_fire() rather than fire_spike().
- Normalized the aiming direction in Trap_ShootHook() rather than Fire_Trap_Hook().
- Removed "start in solid" check/fix from Fire_Trap_Hook().
- firebomb->movetype = MOVETYPE_FLYMISSILE.
- Physics hacks to handle weapon projectiles and C4/Traps touching each other.
- Fixed C4_Touch() when 2 C4s touch ('other' is a freed edict).
- Added (int) typecasts to various cvar flags.
- Removed PlayerNoise() (any calls were inlined), and PNOISE_* #defines.
- Shockbolt entities are now freed upon impact.
- Players can now become Observers whilst noclipped.
- Added sanity checks for *limit cvars.
- Added AMMO_NULL type to ammo_t.
- Added powerup_t enum, and used it for optimisation (to replace strcmps).
- Removed FL_ROBOT flag.
- Replaced Q_Stricmp calls with ent->die (c4/traps), item->weapmodel, item->tag amd item->flags
  integer comparisions.
- Quad sound is now played for the AGM/CD.


-------------------
v2.02-beta.2
-------------------
- Increased CS damage to 50 per 0.1 secs (from 10); increased range to 48 (from 40).
- A player's entity sound is removed when they disconnect.
- Increased the multi-spike reload time by 0.2 seconds.
- In multi-spike mode, spikes are now fired as soon as the fire-button is released.
- In multi-spike mode, a player's rocket count decreases as each spike is loaded.
- Ammo types which are disabled are no longer given in bandoliers or
  backpacks (including Traps and C4).


-------------------
v2.02-beta.3
-------------------
- In multi-spike mode, spikes cannot be fired straight after a previous burst has been fired.
- Fixed bug where traps stuck to rotating entities would sometimes fail to engage their tractor-beams.
- Prevented ESG mode-switching whilst the gun is still "powered up".
- Weapon damage tweaks:
	* Spikes to 25 + 100 from 50 + 115.
	* C4 to 120 from 150.
	* Disc damage to 75 from 50.
- Forced a maximum lifespan on proxies (30 secs), rather than allowing c4_proximity_life = 0 to disable it.
- Decreased the live_time of shock bolts to 3 secs.
- Decreased the targetting range of the homing plasma to 250.
- Decreased the live_time of discs to 2.5 secs.
- The AGM floor-smashing damage is significantly scaled down if > 100.
- A quadded AGM-CD depletes the charge at twice the normal rate.
- T_Damage() calls moved to after "ent->burning = true" assignments in an effort to fix
  "Flame_Expire() called for a non-flame edict" bugs.
- Chainsaw now destroys player's own C4s and Traps.
- Moved var declarations for player's own C4/Traps inside if()s in fire_lead() and fire_rail().
- Replaced a strncmp() with an int compare (sky surface) in fire_lead().
- 'ctfgame' replaced with 'teamgame'; 'g_ctf.c' -> 'g_team.c'; 'g_ctf.h' -> 'g_team.h'
- Replaced Q_Stricmp calls in G_SetClientSound() with ->weapmodel integer comparisions.
- Replaced various Q_Stricmp calls with item->tag and item->weapmodel integer comparisions.
- Added VWep support.
- Removed a "grenade" classname strcmp check in trigger_push_touch().


-------------------
v2.02-beta.4
-------------------
- Added sanity checks where Trap_Die() is called.


-------------------
v2.03-beta.2
-------------------
- Changed all gi.cprintf() to gi_cprintf().
- Changed all gi.centerprintf() to gi_centerprintf().
- Changed all gi.bprintf() to gi_bprintf().
- reserved value limited to maxclients.
- Various bot code bugfixes, eg. logic of & operators.
- Various bot tweaks, eg. trap targetting, jump-pad use, weapon mode selection, etc.
- Separate obit for holding on to trap too long.


-------------------
v2.03-beta.3
-------------------
- Vote number requirements ignore bots when counting.
- Tweaked server commands.
- Bots don't use Flamethrowers underwater.
- Bots use AGM/Cellular Disruptor.
- Fixed bug where bot-targeting routines would attempt to perform operations on
  null ent->client pointers (as traps - now valid targets - have a null client member).
- Fixed bug where client->pers.botindex was reset to 0 upon respawning.
- Fixed bug where SpawnBot_Think() would be caught in an infinite loop.
- Removed "if ((*gi.argv(1) < '0') && (*gi.argv(1) > '9'))" checks.
- Bots added and removed at rate of 1 / 0.1secs (avoids overflows).


-------------------
v2.03-beta.4
-------------------
- Added hack to get round a nameless union in 'bot.h' for the Linux compiler.


-------------------
v2.03-beta.5
-------------------
- Fixed some missing bot jump sounds.
- Bot falling impact sounds now played correctly.
- Bot z-velocity no longer limited when being manipulated by the AGM.
- Fixed bug where if a 'gamemap' command was issued from the console with bots present, the 
  appropriate entries in the player slots array would not be cleared, thus erroneously 
  reducing the number of slots available.
- Bots can use Personal Teleporters if in danger (eg. fallen into lava/slime, sailing into
  the void, about to be gunned down, etc).
- Bot_Think() tweaks.
- Players can call themselves "player" if they want!
- Tracers are freed immediately upon touching something.
- Replaced a "func_monitor" string comparison in ClientThink() with an int comparison.
- Entity state effects are fully cleared during client disconnections, and failed connections.
- Corrected z-coordinate endpoint positions in HazardCheck() and BankCheck().


-------------------
v2.03-beta.6
-------------------
- Fixed bug where bots that died in water would not have gravity applied to their dead bodies 
  or gibbed heads.
- Fixed bug where the presence of bot entities from a previous map would cause the server to hang if
  (a) the new map contained target_lasers, and (b) the r1q2.exe 'sv_gamedebug' cvar was set to 1.
- Changed bot cleanup code to include the case where a "gamemap" command has been used directly.
- Fixed bug where "gamemap" commands would results in blank player names in the scoreboard.
- Fixed redundant updates of configstrings that occurred when bots were added.


-------------------
v2.03-beta.7
-------------------
- Added voting menu options for adding and removing bots.
- Modified "vote <type>" command handler to include bot voting.
- Added route file creation capability (plus "sv savechain", "chedit" and "undo" commands).
- Increased bot swimming speed to equal player's speed.
- Fixed various bugs in G_FindTrainTeam() that caused incorrect chaining.
- Fixed bug in droptofloor2() with a bot navigation waypoint item.
- Fixed bot-jiggling that sometimes occurred at brush edges (restored Pon's original code for 
  the (x >= 70) condition in Bot_AI()).
- Fixed bug in ReadRouteFile() where only the first node in the Route array was cleared before reading
  the route file, rather than the whole array. This caused a server crash during target setting in 
  Bot_AI(), after a 'gamemap' command had been used to load certain maps with fewer route nodes.
- Fixed empty icon tags in the bot navigation items to prevent null indexes being passed to SV_Find().
- Performed various ClientThink() optimisations (Q_stricmp checks -> int checks).
- Replaced various malloc() and free() calls with gi.TagMalloc() and gi.TagFree(), respectively.
- Fixed bug with playerlist menu code for multiple pages.


-------------------
v2.03-beta.8
-------------------
- Added ugly hack to fix erroneous low bot damage from AGM-smashing.
- Added new server command "sv listbots", which displays active bot information.


-------------------
v2.03-beta.9
-------------------
- Team scores in Team-DM and Assault modes now have 3 digits.
- Cvar 'allow_bots' is now latched (v2.03-beta.9).
- AwakenBot voting menu entry and "vote addbots" command are now disabled in appropriate
  circumstances (eg. for CTF and Assault games).
- Bot route file is only read for FFA and Team-DM games.
- Bot team skin/model is now set correctly for Team-DM games when DF_FORCEJOIN is not set.
- Bots now cannot be added or removed during matches.
- Bots automatically set themselves to the 'ready' state for matches.
- Fixed bug where bots left their old model behind after being swapped to the other team.
- Fixed bug where bots failed to join a team for a match.
- Fixed bug where bots tried to play invalid death animation frames when a match started.
- Any bots present are removed when a match is voted to start.
- Player's team info is now reset when they disconnect.
- Added JoinTeam() sanity checks to Op_SwapTeam().


-------------------
v2.04-beta.1
-------------------
Bots:
- Added "show_botroute" command for debugging (small routefiles only).
- Replaced 'enewep' refs with a single 'enemy_wep' call in Combat_Normal() (and similarly elsewhere).
- Fixed FIRE_PRESTAYFIRE handling.
- Fixed FIRE_STAYFIRE handling.
- Fixed FIRE_AVOIDEXPLO handling.
- Fixed FIRE_AVOIDINVULN handling.
- Fixed FIRE_QUADUSE handling.
- Fixed FIRE_JUMPROC handling.
- Tweaked FIRE_RUSH handling.
- Tweaked FIRE_REFUGE handling (and fixed a conditional-grammar bug).
- Tweaked FIRE_ESTIMATE handling re. weapons.
- Tweaked fire handling when on a moving door/plat/train and enemy is far below.
- Added FIRE_C4 handling.
- Added FIRE_C4USE handling.
- Tweaked SkillLevel[] table settings.
- Changed weapon engagement distances for certain weapons (also removed minimum distances for some).
- Changed weapon use preference order (eg. bots now use C4 at close range in preference to the DE).
- Fixed the case lists in GetAimAngle() so that hitscan and projectile weapons are sorted properly.
- Ranges for accuracy, skill and agression are now all [0..9].
- Removed 'TargetIndex' references (from old "sv dsp" command).
- Fixed bug with bots hearing impacts (was checking 'mynoise' instead of 'mynoise2').
- Fixed bug with bots reponding to noises with FIRE_ESTIMATE.
- Tweaked handling of enemy assignment in Bot_CheckEnemy().
- Tweaked Set_Combatstate() logic for NULL and dead targets.
- Primary weapon tried first in Combat_Normal().
- Traps tossed ahead if enemy cannot be seen.

General:
- Enemy tractored status is now cleared in Trap_Die().


-------------------
v2.04-beta.2
-------------------
- Removed unrequired SpawnNumBots() function (always use SpawnNumBots_Safe()).
- Removed 'changetime' from gclient_s struct, and corresponding parameter randomisation code at the start of Bot_AI().
- Fixed some rand() modulus values in RandomizeParameters() so they are [0..9] not [0..8].


-------------------
v2.04-beta.3
-------------------
- Removed all 'userinfo_set' code to fix the hand-position bug.


-------------------
v2.04-beta.4
-------------------
- Tweaked the bot chat and insult parameters.


-------------------
v2.04-beta.5
-------------------
- Added sanity check & debug info to GetKindWeapon().


-------------------
v2.04-beta.6
-------------------
- Added sanity check to Set_Combatstate() for NULL client->enemy pointer.


-------------------
v2.05-beta.1
-------------------
- Various fixes by r1ch (eg. name change crash).
- Fixed various dangerous sprintf() calls.
- Fixed various dangerous fscanf() calls.
- Added string length checks and limits to LoadBotConfig().
- Added unicastSound() function, and replaced the appropriate StuffCmd() calls.
- Added case 5 to Cmd_WeaponNote_f().
- Variables 's.modelindex2-4' now cleared in ClientConnect().
- Variable 'ent->s.solid' now cleared in ClientDiconnect().


-------------------
v2.05-beta.2
-------------------
- Fixed overridden svflags value in Fire_Firebomb().
- Stationary Traps now flagged as SVF_DEADMONSTER and SVF_PROJECTILE in Trap_Touch().
- Added better method of obtaining player's IP address (SNX).
